# ACCESS_CONTROLL_BACKEND

# APP -  `ACCESS_CONTROLL_BACKEND`
==============================================

**Install Dependencies.**   
- cd **_Ms.AccessControl.Core_**
- run **_donet restore_**

**Run Migrations.**   
- create bd **_accesscontrol_**
- delete all files folder **_Migrations_** from ___Ms.AccessControl.Core/Migrations___ 
- run for create migrations **_dotnet ef migrations add EmployyeMigration   --context AccessControlDbContext_**
- update database **dotnet ef database update  --context AccessControlDbContext**

**Run Project.** 
- cd **_Ms.AccessControl.Core_**
- run **_donet run_**
