using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Builder;
using Ms.AccessControl.Core.Data;
using Ms.AccessControl.Core.Models;

namespace Ms.AccessControl.Core
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddLogging(logging =>
            {
                logging.AddConsole();
                logging.AddDebug();
            });

            services.AddCors(options =>
            {
                options.AddPolicy(
                    "AllowAny",
                    x =>
                    {
                        x.AllowAnyHeader()
                        .AllowAnyMethod()
                        .SetIsOriginAllowed(isOriginAllowed: _ => true)
                        .AllowCredentials();
                    });
            });
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            }).AddNewtonsoftJson();

            services.AddAuthorization();
            services.AddOData();
            services.AddODataQueryFilter();
            services.AddHttpContextAccessor();
            services.AddDbContext<AccessControlDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("dataSource"),
                   b => b.MigrationsAssembly("AccessControl"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            IServiceProvider provider = app.ApplicationServices.GetRequiredService<IServiceProvider>();
            app.UseCors("AllowAny");
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            app.UseMvc(builder =>
            {
                builder.Count().Filter().OrderBy().Expand().Select().MaxTop(null).SetTimeZoneInfo(TimeZoneInfo.Utc);

                if (env.EnvironmentName == "Development")
                {
                    builder.MapRoute(name: "default",
                        template: "{controller}/{action}/{id?}",
                        defaults: new { controller = "Home", action = "Index" }
                    );
                }
                var oDataBuilder = new ODataConventionModelBuilder(provider);
                oDataBuilder.EntitySet<Employee>("Employees");
                var functionViewDistribuidor = oDataBuilder.EntityType<Employee>()
                .Collection.Function("SearchEmployees")
                .ReturnsCollectionFromEntitySet<Employee>("Employees");
                functionViewDistribuidor.Namespace = "EmployeesService";
                functionViewDistribuidor.Parameter<string>("query");

                var model = oDataBuilder.GetEdmModel();
                builder.MapODataServiceRoute("odata/accesscontrol", "odata/accesscontrol", model);
            });
        }
    }
}
