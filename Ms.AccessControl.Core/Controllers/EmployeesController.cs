using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Ms.AccessControl.Core.Models;
using Ms.AccessControl.Core.Data;
using System.Globalization;

namespace Ms.AccessControl.Core.Controllers
{

  [ODataRoutePrefix("odata/accesscontrol/Employees")]
  [Route("mvc/odata/accesscontrol/Employees")]
  public partial class EmployeesController : ODataController
  {
    private AccessControlDbContext context;

    public EmployeesController(AccessControlDbContext context)
    {
      this.context = context;
    }
    // GET /odata/accesscontrol/Employee
    [EnableQuery(MaxExpansionDepth=10,MaxAnyAllExpressionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<Employee> GetEmployees()
    {
      var items = this.context.Employees.AsQueryable<Employee>();
      return items;
    }
    [EnableQuery(MaxExpansionDepth=10,MaxAnyAllExpressionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<Employee> GetEmployee(int key)
    {
        var items = this.context.Employees.Where(i=>i.id == key);
        return SingleResult.Create(items);
    }

    partial void OnEmployeeDeleted(Employee item);
    [HttpDelete("{Id}")]
    public IActionResult DeleteEmployee(int key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var items = this.context.Employees
                .Where(i => i.id == key)
                .AsQueryable();
            items = EntityPatch.ApplyTo<Employee>(Request, items);

            var itemToDelete = items.FirstOrDefault();

            if (itemToDelete == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnEmployeeDeleted(itemToDelete);
            this.context.Employees.Remove(itemToDelete);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxAnyAllExpressionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchEmployee(int key, [FromBody]Delta<Employee> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.Employees.Where(i => i.id == key);

            items = EntityPatch.ApplyTo<Employee>(Request, items);

            var itemToUpdate = items.FirstOrDefault();

            if (itemToUpdate == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(itemToUpdate);
            this.context.Employees.Update(itemToUpdate);
            this.context.SaveChanges();
            var itemToReturn = this.context.Employees.Where(i => i.id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxAnyAllExpressionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] Employee item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }
            this.context.Employees.Add(item);
            this.context.SaveChanges();
            var key = item.id;
            var itemToReturn = this.context.Employees.Where(i => i.id == key);
            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    //Functions CUSTOM
    //===============================================================================
    // http://localhost:5000/odata/accesscontrol/Employees/EmployeesService.SearchEmployees(query=${query})
    [HttpGet]
    [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
    public IEnumerable<Employee> SearchEmployees([FromODataUri] string query)
    {
        var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
        var items = this.context.Employees.AsQueryable();
        if(!string.IsNullOrEmpty(query)){
            items =  items.AsParallel().Where(a => 
            a.firstName.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) ||
            a.otherNames.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) || 
            a.surname.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) ||
            a.secondSurname.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) ||
            a.idType.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) ||
            a.country.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase ) ||
            a.email.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase) ||
            a.state.ToLower().RemoveDiacritics().Contains(query.ToLower(),StringComparison.InvariantCultureIgnoreCase)
            ).AsQueryable();
        }            
        return items;
    }
  }
}
