﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.AccessControl.Core.Migrations
{
    public partial class EmployyeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateSequence(
                name: "Employee_seq",
                schema: "public");

            migrationBuilder.CreateTable(
                name: "Employee",
                schema: "public",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false, defaultValueSql: "nextval('\"Employee_seq\"'::regclass)"),
                    surname = table.Column<string>(nullable: true),
                    secondSurname = table.Column<string>(nullable: true),
                    firstName = table.Column<string>(nullable: true),
                    otherNames = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true, defaultValueSql: "'Colombia'"),
                    idType = table.Column<string>(nullable: true),
                    identificationNumber = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    admissionDate = table.Column<DateTime>(nullable: false),
                    area = table.Column<string>(nullable: true),
                    state = table.Column<string>(nullable: true, defaultValueSql: "'Activo'"),
                    registrationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee",
                schema: "public");

            migrationBuilder.DropSequence(
                name: "Employee_seq",
                schema: "public");
        }
    }
}
